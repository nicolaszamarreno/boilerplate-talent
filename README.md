# Configuration

_A little boilerplate for basic integration_

## Installation

### Package NPM

```bash
$ npm install
```

### Code with liveReload Configuration

_You are ready for coding_

```bash
$ npm run watch
```

### Launch your navigator

_Your page wait you in the localhost:8080, let's go !_  
Now, you can change your .Less and code your structure HTML without refresh your browser! Amazing!

#### Enjoy <3
