const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebpackNotifierPlugin = require("webpack-notifier");
const webpack = require("webpack");

module.exports = {
    context: path.resolve(__dirname, "./"),
    entry: { main: "./src/index.js", html: "./src/index.html" },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "./bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.less/,
                use: [
                    "style-loader",
                    "css-hot-loader",
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "less-loader"
                ]
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: "html-loader"
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf|svg)$/,
                include: /node_modules/,
                loader: "file-loader",
                options: {
                    name: "./assets/fonts/[name].[ext]",
                    publicPath: ""
                }
            },
            {
                test: /\.(svg|png|jpg|gif)$/,
                loader: "file-loader",
                options: {
                    name: "./assets/images/[name].[ext]",
                    publicPath: ""
                }
            }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx", ".json", ".less", ".ts", ".tsx"],
        modules: [path.resolve("./node_modules")]
    },
    stats: { colors: true },
    devServer: {
        hot: true,
        contentBase: ["./src/"],
        watchContentBase: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new WebpackNotifierPlugin(),
        new MiniCssExtractPlugin({
            filename: "[name].css"
        }),
        new HtmlWebpackPlugin({
            inject: true,
            hash: true,
            template: "./src/index.html",
            filename: "index.html"
        })
    ]
};
